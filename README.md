# olvap dotfiles

```sh
sudo apt-get install git
```

Run this:

```sh
git clone git@gitlab.com:olvap/dotfiles.git ~/.dotfiles
cd ~/.dotfiles
script/bootstrap
```

This will symlink the appropriate files in `.dotfiles` to your home directory.
Everything is configured and tweaked within `~/.dotfiles`.
