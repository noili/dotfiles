function! myspacevim#before() abort
  highlight ColorColumn ctermbg=blue
  call matchadd('ColorColumn', '\%81v', 100)
  map <F5> :w <CR>
  map <F6> :Gstatus <CR>
  map <F8> Obinding.pry<Esc>
  imap <F5> <Esc><F5>

  " spell check
  map <F7> :setlocal spell! spelllang=es<CR>
  " next word
  nmap <F9> ]s
  " Autofix word
  nmap <F10> 1z=
  " ----------
endfunction
