curl -sLf https://spacevim.org/install.sh | bash
ln -s ~/.dotfiles/spacevim/init.toml ~/.SpaceVim.d/init.toml
mkdir ~/.SpaceVim/autoload
ln -s ~/.dotfiles/spacevim/myspacevim.vim ~/.SpaceVim.d/autoload/myspacevim.vim
